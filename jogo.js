var jogador = 0;
var jogadas = 0;

function clique(id) {
	console.log("jogador: "+jogador);
	
	var tag = null;
	
	if(id.currentTarget)
		tag = id.currentTarget; /* currentTarget é para obter o elemento que foi alvo do evento */
	else
		tag = document.getElementById('casa'+id);

	if(tag.style.backgroundImage == '' || tag.style.backgroundImage == null)
	{
		var endereco = 'img/'+jogador+'.jpg';
		tag.style.backgroundImage = 'url('+endereco+')';
		jogadas += 1;

		var ganhou = 0
		if (jogadas >= 5) {
		   ganhou = verificarGanhador();
		   }
		switch(ganhou)
		{
			case 1:
				if(jogador == 0)
				result.innerHTML = 'Mônica Ganhou!';
				else
				result.innerHTML = 'Cebolinha Ganhou!';
				break;
			case -1:
				result.innerHTML = 'Deu velha!';
				break;
			case 0:
				// ninguem ganhou ainda
				break;
		}

		vez = document.getElementById('vez');
		if(jogador == 0)
		{
			jogador = 1;
			vez.innerHTML = 'Cebolinha';
		}else
		{
			jogador = 0;
			vez.innerHTML = 'Mônica';
		}

		if(ganhou!=0)
		{
			finalizar();
		}
	}
}

function verificarGanhador(){
	// pegar elementos
	c1 = document.getElementById('casa1');
	c1 = c1.style.backgroundImage;
	c2 = document.getElementById('casa2');
	c2 = c2.style.backgroundImage;
	c3 = document.getElementById('casa3');
	c3 = c3.style.backgroundImage;
	c4 = document.getElementById('casa4');
	c4 = c4.style.backgroundImage;
	c5 = document.getElementById('casa5');
	c5 = c5.style.backgroundImage;
	c6 = document.getElementById('casa6');
	c6 = c6.style.backgroundImage;
	c7 = document.getElementById('casa7');
	c7 = c7.style.backgroundImage;
	c8 = document.getElementById('casa8');
	c8 = c8.style.backgroundImage;
	c9 = document.getElementById('casa9');
	c9 = c9.style.backgroundImage;

	if( (c1==c2 && c2==c3 && c1!='') ||
		(c4==c5 && c5==c6 && c4!='') ||
		(c7==c8 && c8==c9 && c7!='') ||
		(c1==c4 && c4==c7 && c1!='') ||
		(c2==c5 && c5==c8 && c2!='') ||
		(c3==c6 && c6==c9 && c3!='') ||
		(c1==c5 && c5==c9 && c1!='') ||
		(c3==c5 && c5==c7 && c3!=''))
	{
		return 1; // ganhou

	}else if(jogadas == 9){
		return -1;
	}
	return 0;
}

function finalizar(){
	tags = document.getElementsByClassName('casa');

	for(i=0; i<9; i++){
		tags[i].onclick = null;
	}
}

function iniciar() {
    result.innerHTML = ' ';
	jogador = jogador == 0 ? 1 : 0;;
	jogadas = 0;

	casas = document.getElementsByClassName('casa');

	for(i=0; i<9; i++){
		casas[i].onclick=clique;
		casas[i].style.backgroundImage='';
	}

	if(jogador == 0)
		{
			jogador = 1;
			vez.innerHTML = 'Cebolinha';
		}else
		{
			jogador = 0;
			vez.innerHTML = 'Mônica';
		}

}



// 1- verificar ganhador OK
// 2- exibir resultado OK
// 3- Mensagem quando der velha. OK
// 4- Indicar o jogador da vez - OK
// 5- Iniciar nova partida - OK
// 6- Finalização do jogo - OK
// 7- Placar (usando localStorage) https://www.w3schools.com/jsref/prop_win_localstorage.asp

/**
Itens do Trabalho (individual)
1- Melhorar quando se começa a verificar ganhador (a partir da 5 jogada) OK
2- Melhorar a verificação das linhas para saber se alguém ganhou...
 reduzir de 8 verificações para o mínimo.
4- Trocar alert por mensagem de texto no html. OK
5- Definir personagens  antagonistas e visual harmonico para o tabuleiro (css) OK
*/
